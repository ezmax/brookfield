# Ezmax\Brookfield\TransactionApi

All URIs are relative to *http://frs-qa.brookfieldres.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**transactionAddUpdateTransaction**](TransactionApi.md#transactionAddUpdateTransaction) | **POST** /api/transaction | 
[**transactionDeleteTransaction**](TransactionApi.md#transactionDeleteTransaction) | **POST** /api/deletetransaction | 
[**transactionGetAgents**](TransactionApi.md#transactionGetAgents) | **GET** /api/getagents | 
[**transactionGetCodeData**](TransactionApi.md#transactionGetCodeData) | **GET** /api/codedata | 
[**transactionGetTransaction**](TransactionApi.md#transactionGetTransaction) | **GET** /api/gettransaction/{id} | 
[**transactionGetTransactions**](TransactionApi.md#transactionGetTransactions) | **GET** /api/gettransactions/{PageNumber}/{PageSize}/{TradeNumber}/{Year}/{Month} | 


# **transactionAddUpdateTransaction**
> \Ezmax\Brookfield\Model\FRSTransaction transactionAddUpdateTransaction($transaction)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Ezmax\Brookfield\Api\TransactionApi();
$transaction = new \Ezmax\Brookfield\Model\FRSTransaction(); // \Ezmax\Brookfield\Model\FRSTransaction | 

try {
    $result = $api_instance->transactionAddUpdateTransaction($transaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionAddUpdateTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction** | [**\Ezmax\Brookfield\Model\FRSTransaction**](../Model/\Ezmax\Brookfield\Model\FRSTransaction.md)|  |

### Return type

[**\Ezmax\Brookfield\Model\FRSTransaction**](../Model/FRSTransaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionDeleteTransaction**
> bool transactionDeleteTransaction($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Ezmax\Brookfield\Api\TransactionApi();
$id = 56; // int | 

try {
    $result = $api_instance->transactionDeleteTransaction($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionDeleteTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionGetAgents**
> \Ezmax\Brookfield\Model\TransactionAgent[] transactionGetAgents()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Ezmax\Brookfield\Api\TransactionApi();

try {
    $result = $api_instance->transactionGetAgents();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionGetAgents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Ezmax\Brookfield\Model\TransactionAgent[]**](../Model/TransactionAgent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionGetCodeData**
> \Ezmax\Brookfield\Model\CodeData[] transactionGetCodeData()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Ezmax\Brookfield\Api\TransactionApi();

try {
    $result = $api_instance->transactionGetCodeData();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionGetCodeData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Ezmax\Brookfield\Model\CodeData[]**](../Model/CodeData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionGetTransaction**
> \Ezmax\Brookfield\Model\FRSTransaction transactionGetTransaction($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Ezmax\Brookfield\Api\TransactionApi();
$id = 56; // int | 

try {
    $result = $api_instance->transactionGetTransaction($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionGetTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\Ezmax\Brookfield\Model\FRSTransaction**](../Model/FRSTransaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionGetTransactions**
> \Ezmax\Brookfield\Model\TupleIEnumerableTransactionHeaderIEnumerableTransactionTotals transactionGetTransactions($page_number, $page_size, $trade_number, $year, $month)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Ezmax\Brookfield\Api\TransactionApi();
$page_number = 56; // int | 
$page_size = 56; // int | 
$trade_number = "trade_number_example"; // string | 
$year = 56; // int | 
$month = 56; // int | 

try {
    $result = $api_instance->transactionGetTransactions($page_number, $page_size, $trade_number, $year, $month);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionGetTransactions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_number** | **int**|  |
 **page_size** | **int**|  |
 **trade_number** | **string**|  |
 **year** | **int**|  |
 **month** | **int**|  |

### Return type

[**\Ezmax\Brookfield\Model\TupleIEnumerableTransactionHeaderIEnumerableTransactionTotals**](../Model/TupleIEnumerableTransactionHeaderIEnumerableTransactionTotals.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

