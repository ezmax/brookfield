# TupleIEnumerableTransactionHeaderIEnumerableTransactionTotals

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_item1** | [**\Ezmax\Brookfield\Model\TransactionHeader[]**](TransactionHeader.md) |  | [optional] 
**m_item2** | [**\Ezmax\Brookfield\Model\TransactionTotals[]**](TransactionTotals.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


