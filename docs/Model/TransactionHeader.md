# TransactionHeader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**te_header_id** | **int** |  | [optional] 
**street_address** | **string** |  | [optional] 
**street_address2** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**postal_code** | **string** |  | [optional] 
**province** | **int** |  | [optional] 
**trade_number** | **string** |  | [optional] 
**transaction_type** | **int** |  | [optional] 
**sale_price** | **double** |  | [optional] 
**closed_finalized_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**paid_finalized_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**total_gross_commission** | **double** |  | [optional] 
**transaction_end** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


