# TransactionAgent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent_name** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**company_person_id** | **int** |  | [optional] 
**back_office_number** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


