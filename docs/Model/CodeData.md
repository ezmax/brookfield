# CodeData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute** | **string** |  | [optional] 
**ct_code** | **int** |  | [optional] 
**descr** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


