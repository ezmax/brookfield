# FRSTransaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_header** | [**\Ezmax\Brookfield\Model\TransactionHeader**](TransactionHeader.md) |  | [optional] 
**transaction_details_sale** | [**\Ezmax\Brookfield\Model\TransactionDetail[]**](TransactionDetail.md) |  | [optional] 
**transaction_details_buy** | [**\Ezmax\Brookfield\Model\TransactionDetail[]**](TransactionDetail.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


