# TransactionDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**te_detail_id** | **int** |  | [optional] 
**te_header_id** | **int** |  | [optional] 
**company_person_id** | **int** |  | [optional] 
**agent_role** | **string** |  | [optional] 
**gross_commission** | **double** |  | [optional] 
**transaction_end** | **int** |  | [optional] 
**deleted** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


