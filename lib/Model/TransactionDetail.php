<?php
/**
 * TransactionDetail
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ezmax\Brookfield
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * FRS API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Ezmax\Brookfield\Model;

use \ArrayAccess;

/**
 * TransactionDetail Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Ezmax\Brookfield
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class TransactionDetail implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'TransactionDetail';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'te_detail_id' => 'int',
        'te_header_id' => 'int',
        'company_person_id' => 'int',
        'agent_role' => 'string',
        'gross_commission' => 'double',
        'transaction_end' => 'int',
        'deleted' => 'bool'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'te_detail_id' => 'TEDetailID',
        'te_header_id' => 'TEHeaderID',
        'company_person_id' => 'CompanyPersonID',
        'agent_role' => 'AgentRole',
        'gross_commission' => 'GrossCommission',
        'transaction_end' => 'TransactionEnd',
        'deleted' => 'Deleted'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'te_detail_id' => 'setTeDetailId',
        'te_header_id' => 'setTeHeaderId',
        'company_person_id' => 'setCompanyPersonId',
        'agent_role' => 'setAgentRole',
        'gross_commission' => 'setGrossCommission',
        'transaction_end' => 'setTransactionEnd',
        'deleted' => 'setDeleted'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'te_detail_id' => 'getTeDetailId',
        'te_header_id' => 'getTeHeaderId',
        'company_person_id' => 'getCompanyPersonId',
        'agent_role' => 'getAgentRole',
        'gross_commission' => 'getGrossCommission',
        'transaction_end' => 'getTransactionEnd',
        'deleted' => 'getDeleted'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['te_detail_id'] = isset($data['te_detail_id']) ? $data['te_detail_id'] : null;
        $this->container['te_header_id'] = isset($data['te_header_id']) ? $data['te_header_id'] : null;
        $this->container['company_person_id'] = isset($data['company_person_id']) ? $data['company_person_id'] : null;
        $this->container['agent_role'] = isset($data['agent_role']) ? $data['agent_role'] : null;
        $this->container['gross_commission'] = isset($data['gross_commission']) ? $data['gross_commission'] : null;
        $this->container['transaction_end'] = isset($data['transaction_end']) ? $data['transaction_end'] : null;
        $this->container['deleted'] = isset($data['deleted']) ? $data['deleted'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets te_detail_id
     * @return int
     */
    public function getTeDetailId()
    {
        return $this->container['te_detail_id'];
    }

    /**
     * Sets te_detail_id
     * @param int $te_detail_id
     * @return $this
     */
    public function setTeDetailId($te_detail_id)
    {
        $this->container['te_detail_id'] = $te_detail_id;

        return $this;
    }

    /**
     * Gets te_header_id
     * @return int
     */
    public function getTeHeaderId()
    {
        return $this->container['te_header_id'];
    }

    /**
     * Sets te_header_id
     * @param int $te_header_id
     * @return $this
     */
    public function setTeHeaderId($te_header_id)
    {
        $this->container['te_header_id'] = $te_header_id;

        return $this;
    }

    /**
     * Gets company_person_id
     * @return int
     */
    public function getCompanyPersonId()
    {
        return $this->container['company_person_id'];
    }

    /**
     * Sets company_person_id
     * @param int $company_person_id
     * @return $this
     */
    public function setCompanyPersonId($company_person_id)
    {
        $this->container['company_person_id'] = $company_person_id;

        return $this;
    }

    /**
     * Gets agent_role
     * @return string
     */
    public function getAgentRole()
    {
        return $this->container['agent_role'];
    }

    /**
     * Sets agent_role
     * @param string $agent_role
     * @return $this
     */
    public function setAgentRole($agent_role)
    {
        $this->container['agent_role'] = $agent_role;

        return $this;
    }

    /**
     * Gets gross_commission
     * @return double
     */
    public function getGrossCommission()
    {
        return $this->container['gross_commission'];
    }

    /**
     * Sets gross_commission
     * @param double $gross_commission
     * @return $this
     */
    public function setGrossCommission($gross_commission)
    {
        $this->container['gross_commission'] = $gross_commission;

        return $this;
    }

    /**
     * Gets transaction_end
     * @return int
     */
    public function getTransactionEnd()
    {
        return $this->container['transaction_end'];
    }

    /**
     * Sets transaction_end
     * @param int $transaction_end
     * @return $this
     */
    public function setTransactionEnd($transaction_end)
    {
        $this->container['transaction_end'] = $transaction_end;

        return $this;
    }

    /**
     * Gets deleted
     * @return bool
     */
    public function getDeleted()
    {
        return $this->container['deleted'];
    }

    /**
     * Sets deleted
     * @param bool $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->container['deleted'] = $deleted;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Ezmax\Brookfield\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Ezmax\Brookfield\ObjectSerializer::sanitizeForSerialization($this));
    }
}


